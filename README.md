[TOC]

# Overview

Promises based version of [xhr.js](https://bitbucket.org/workslon/xhr.js) UMD library with the `default` extension which allows you to preset the default values (e.g. request headers) for all your further requests. See [Examples](#markdown-header-examples) section for the possible usage.

Dependency: `parole.js`

# Install from NPM

```shell
npm install git+ssh://git@bitbucket.org/workslon/xhr-parole.git --save
```

# Usage

## Browser

+ Add [`parole.js`](https://bitbucket.org/workslon/parole) and `xhr-parole.js` libraries to the page via `<script>` tag

+ `window.XHR` variable will be available for you to use

## CommonJS

+ Run `npm install https://git@bitbucket.org/workslon/xhr-parole.js.git`

or `git+ssh://git@bitbucket.org/workslon/xhr-parole.git`

in the command-line - this will install the module and add it to the `node_modules` folder of your project

+ `var XHR = require('xhr-parole');` from any file

## AMD (RequireJS)

+ Add [`parole.js`](https://bitbucket.org/workslon/parole) library to the project

+ Add `xhr-parole.js` library to the project

+ Config RequireJS

+ `var XHR = require('xhr-parole');`

# Examples

### GET

```javascript
var xhr = new XHR();

xhr.GET({
  url: 'https://api.github.com/gists/706e4899b785f4f9fc1a'
})
.then(
  function (result) { // success
    console.log(result);
  },

  function (error) { // fail
    console.error(error);
  }
)
```

## GET with Default Headers

```javascript
var xhr = new XHR();

xhr.defaults = {
  headers: {
    'X-Parse-Application-Id': 'XXX',
    'X-Parse-REST-API-Key': 'XXX'
  }
}

xhr.GET({
  url: 'https://api.github.com/gists/706e4899b785f4f9fc1a'
})
.then(
  function (result) { // success
    console.log(result);
  },

  function (error) { // fail
    console.error(error);
  }
)
```

# API References

## Properties:

### defaults

Preset default values to be used in every request.

__NOTE:__ will be overriden by the `params` if provided

## Methods:

### GET(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

### POST(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

### PUT(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

### DELETE(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

### OPTIONS(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

## Available Parameters

| Name            | Type      | Description                                             |   |
|-----------------|-----------|---------------------------------------------------------|---|
| url             | String    | request URL                                             |   |
| headers         | Object    | request Headers                                         |   |
| data            | Object    | data to be sent (relevant to 'POST' and 'PUT' requests) |   |
| requestFormat   | String    | request `contentType` header                            |   |
| responseFormat  | String    | request `Accept` header                                 |   |
